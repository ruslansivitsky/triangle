package com.sivitsky.ruslan.model;

/**
 * Created by ruslan on 27/11/14.
 */
public class TriangleModel {

    private double a;
    private double b;
    private double c;
    String exists;

    public TriangleModel(){}

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double getC() {
        return c;
    }

    public void setC(double c) {
        this.c = c;
    }

    public String getExists() {
        return exists;
    }

    public void setExists(String exists) {
        this.exists = exists;
    }

    public TriangleModel(double a, double b, double c) {
        this.a = a;
        this.b = b;

        this.c = c;
    }

    public String toString()
    {
        return "a="+a+", b="+b+", c="+c;
    }
}
