package com.sivitsky.ruslan.service;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by ruslan on 30/11/14.
 */
@Path("/service")
public class TriangleResourse {
    @POST
    @Path("/checkTriangle")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String checkTriangle(String triangleParameters){
        String result;
        result = "{\n" +
                "    \"exists\": \"YES\"\n" +
                "}";
        return result;

    }

    @GET
    @Path("/version")
    public Response getMsg() {

        String output = "Version 1.0-SNAPSHOT";

        return Response.status(200).entity(output).build();

    }
}
