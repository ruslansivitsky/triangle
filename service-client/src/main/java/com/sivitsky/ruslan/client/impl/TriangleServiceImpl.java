package com.sivitsky.ruslan.client.impl;

import com.sivitsky.ruslan.client.TriangleService;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MediaType;

/**
 * Created by ruslan on 28/11/14.
 */
@Service
public class TriangleServiceImpl implements TriangleService {

    private Client client;
    @Value("${base.url}")
    private String BaseUrl;

    public TriangleServiceImpl() {
        client = Client.create();
    }


    @Override
    public String checkTriangle(double a, double b, double c) {


        String jsonStringTriangle = "{"  + "\"a\":" + "\"" + Double.toString(a) +"\","
                + "\"b\":" + "\"" + Double.toString(b) +"\","
                + "\"c\":" + "\"" + Double.toString(c) + "\"" + "}";

        WebResource webResource = client.resource(BaseUrl);

        String result = webResource.accept("application/json").header("Content-Type", MediaType.APPLICATION_JSON_TYPE).post(String.class, jsonStringTriangle);

        return result;
    }
}
