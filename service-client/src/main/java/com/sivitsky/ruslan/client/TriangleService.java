package com.sivitsky.ruslan.client;

/**
 * Created by ruslan on 28/11/14.
 */
public interface TriangleService {
    public String checkTriangle(double a, double b, double c);
}