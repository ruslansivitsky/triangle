package com.sivitsky.ruslan.web;

import com.sivitsky.ruslan.service.TriangleService;

import com.sivitsky.ruslan.web.model.TriangleModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.UnexpectedTypeException;
import javax.validation.Valid;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

/**
 * User: Tanya
 * Date: 09.01.14
 * Time: 1:27
 */
@Controller
public class WelcomeController {
/*
    @Autowired
    private TranslateService translateService;
*/
    @Autowired
    private TriangleService triangleService;

    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String initForm(ModelMap model) {
        TriangleModel triangleModel = new TriangleModel((double)1, (double)2, (double)3);
        model.addAttribute("triangleModel", triangleModel);
        return "index";
    }

    @RequestMapping(value = "/index", method = RequestMethod.POST)
    public String checkIsTriangle(
          @ModelAttribute("triangleModel") TriangleModel triangleModel,
            BindingResult bindingResult)
            throws IOException, NoSuchFieldException, ClassNotFoundException, UnexpectedTypeException {
        if (bindingResult.hasErrors()) {
            return "index";
        }

        String s = triangleService.checkTriangle((double)triangleModel.getA(), (double)triangleModel.getB(), (double)triangleModel.getC());
        triangleModel.setExists(s);
        return "index";
    }

}